 /**
 * This view is an example list of people.
 */
Ext.define('Pertemuan.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    title: 'Data Anggota', 

    requires: [
        'Ext.grid.plugin.Editable',
        //'Pertemuan.store.Personnel'
    ],

    plugins: [{
        type: 'grideditable'
    }],

   

    bind: '{personnel}',
    viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },

    
    
    columns: [
        //{ text: 'User',  dataIndex: 'user_id', width: 100},
        { text: 'Nama',  dataIndex: 'name', width: 230, editable:true },
        { text: 'Email', dataIndex: 'email', width: 230, editable:true },
        { text: 'Telepon', dataIndex: 'phone', width: 150, editable:true }
    ],

    /*listeners: {
        select: 'onDataDilipih'
    }*/
    listeners: {
        select: 'onItemSelected'
    }
});

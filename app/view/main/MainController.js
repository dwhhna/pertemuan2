/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('Pertemuan.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    /*onDataDipilih: function (sender, record) {
        var nama= record.data.name;
        var npm= record.data.npm;
        localStorage.setItem('nama',nama);
        localStorage.setItem('npm',npm);
        Ext.Msg.confirm('Konfirmasi', 'Yakinn '+nama+' ?', 'onConfirm', this);
        console.log(record.data);
    },*/

    onItemSelected: function (sender, record) {
       
        //alert(record.data.name);
        //Ext.getStore('personnel').filter('name', record.data.name)
        Ext.getStore('detailpersonnel').getProxy().setExtraParams({
            user_id: record.data.user_id
        });
        Ext.getStore('detailpersonnel').load();
        Ext.getStore('personnel').remove(record);
        alert("Data Telah dihapus");
    },

    onBarItemSelected: function (sender, record) {
       
        
    },

    /*onConfirm: function (choice) {

        var nama =localStorage.getItem('nama');
        var npm =localStorage.getItem('npm');
        if (choice === 'yes') {
            alert('Terimakasih memilih YESS, '+nama+' ('+npm+')');
            //
        }
        else {
            alert('Jangan memilih NOO, '+nama+' ('+npm+')');

        }
    },*/

    onReadClicked : function(){
        Ext.getStore('personnel').load(); 
    },

    onSimpanPerubahan: function(){
    name= Ext.getCmp('myname').getValue();
    email= Ext.getCmp('myemail').getValue();
    phone= Ext.getCmp('myphone').getValue();
    store= Ext.getStore('personnel');
    record = Ext.getCmp('mydataview').getSelection();
    index= store.indexOf(record);
    record= store.getAt(index);
    store.beginUpdate();
    record.set('name',name);
    record.set('email',email);
    record.set('phone',phone);
    store.endUpdate();
    alert("updating..");
    },

    onTambahPersonnel: function(){
    name= Ext.getCmp('myname').getValue();
    email= Ext.getCmp('myemail').getValue();
    phone= Ext.getCmp('myphone').getValue();

    store= Ext.getStore('personnel');
    store.beginUpdate();
    store.insert(0,{'name' :name, 'email' :email, 'phone' :phone});
    store.endUpdate();
    alert("Inserting..");
    }

});

function onDeletePersonnel(user_id){
    record = Ext.getCmp('mydataview').getSelection();
    Ext.getStore('personnel').remove(record);
    alert(user_id);
};

function onUpdatePersonnel(user_id){
    record = Ext.getCmp('mydataview').getSelection();
    name = record.data.name;
    email = record.data.email;
    phone = record.data.phone;
    Ext.getCmp('myname').setValue(name);
    Ext.getCmp('myemail').setValue(email);
    Ext.getCmp('myphone').setValue(phone);
    
};
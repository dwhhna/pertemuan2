/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Pertemuan.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    id: 'app-main',

    requires: [
        'Ext.MessageBox',
        'Pertemuan.view.main.MainController',
        'Pertemuan.view.main.MainModel',
        'Pertemuan.view.main.List',
        'Pertemuan.view.form.User',
        'Pertemuan.view.group.Carousel',
        'Pertemuan.view.setting.BasicDataView',
        'Pertemuan.view.form.LoginController',
        'Pertemuan.view.form.Login',
        'Pertemuan.view.chart.Colomn',
        'Pertemuan.view.chart.Scatter',
        'Pertemuan.view.chart.ScatterController',
        'Pertemuan.view.chart.Radar',
        'Pertemuan.view.chart.RadarController',
        'Pertemuan.view.treelist.TreePanel',
        'Pertemuan.view.main.DataViewDetail',
        'Pertemuan.view.main.FormEdit',
        'Pertemuan.store.Bar',
        'Pertemuan.view.main.Bar',
        'Pertemuan.view.main.ListBar'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [{
            docked: 'top',
            xtype: 'toolbar',
            items: [
                {
                        xtype: 'button',
                        text: 'Read',
                        ui:'action',
                        scope: this,
                        listeners: {
                            tap:'onReadClicked'
                        }
                }
            ]
        },
        {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'panel',
                layout: 'hbox',
                items: [{
                    xtype: 'mainlist',
                    flex:2
                },{
                    xtype: 'detailview',
                    flex:1
                },{
                    xtype: 'formedit',
                    flex:1
                }]

            }]
        },
        {
            title: 'Users',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype: 'user'
            }]
        },{
            title: 'Bar',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype: 'panel',
                layout: 'vbox',
                items: [{
                    xtype: 'bar',
                    flex:2
                },{
                    xtype: 'listbar',
                    flex:1
                }]

            }]
        },{
            title: 'Groups',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
            items: [{
                xtype: 'mycarousel'
            }]
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype:'basicdataview'
            }]

        },{
            title: 'Chart',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype:'colomnchart'
            }]

        },{
            title: 'Chart2',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype :'scatter1'
            }]

        },{
            title: 'Chart3',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype :'radar'
            }]

        },{
            title: 'Tree',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype :'tree-panel'
            }]

        }
    ]
});

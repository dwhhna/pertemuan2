Ext.define('Pertemuan.view.main.DataViewDetail', {
    extend: 'Ext.Container',
    xtype: 'detailview',
    requires: [
    'Ext.dataview.plugin.ItemTip',
    'Ext.plugin.Responsive',
    'Pertemuan.store.DetailPersonnel'
    ],

    
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,

    /*viewModel: {
        stores: {
            detailpersonnel: {
                type: 'detailpersonnel'
            }
        }
    },*/

    viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },

    items: [{
        xtype: 'dataview',
        scrollable: 'y',
        id:'mydataview',
        cls: 'dataview-basic',
        itemTpl:'<table style="border-spacing:5px; border-collapse:separate">'+ 
        '<td><font size = 4><b>{user_id}</b><br>{name}<br>{email}<br>{phone}<br><button type=button onclick="onUpdatePersonnel({user_id})">Update</button><button type=button onclick="onDeletePersonnel({user_id})">Hapus</button></font ></td>'+
        '<hr>',

        bind: {
           // store: '{detailpersonnel}'
           store: '{personnel}'
       },

       /*plugins: {
        type: 'dataviewtip',
        align: 'l-r?',
        plugins: 'responsive',

            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            //delegate: '.img',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
            '<tr><td>User_id: </td><td>{user_id}</td></tr>' +
            '<tr><td>Nama: </td><td>{name}</td></tr>' +
            '<tr><td>Email: </td><td>{email}</td></tr>' +
            '<tr><td>Phone: </td><td>{phone}</td></tr>' 

        }*/
    }]
});
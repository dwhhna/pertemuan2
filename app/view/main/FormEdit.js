/**
 * Demonstrates a tabbed form panel. This uses a tab panel with 3 tabs - Basic, Sliders and Toolbars - each of which is
 * defined below.
 *
 * See this in action at http://dev.sencha.com/deploy/sencha-touch-2-b3/examples/kitchensink/index.html#demo/forms
 */
Ext.define('Pertemuan.view.main.FormEdit', {
    extend: 'Ext.form.Panel',
    shadow: true,
    xtype : 'formedit',
    id: 'formedit',
    items: [    {
                    xtype: 'textfield',
                    name: 'name',
                    id:'myname',
                    label: 'Name',
                    placeHolder: 'Your Name',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },{
                    xtype: 'emailfield',
                    name: 'email',
                    id:'myemail',
                    label: 'Email',
                    placeHolder: 'me@sencha.com',
                    clearIcon: true
                },{
                    xtype: 'textfield',
                    name: 'phone',
                    id:'myphone',
                    label: 'Phone',
                    placeHolder: '0811111111',
                    clearIcon: true
                },{
                    xtype:'button',
                    ui:'action',
                    text: 'Simpan Perubahan',
                    handler:'onSimpanPerubahan'
                },
                {
                    xtype:'button',
                    ui:'confirm',
                    text: 'Tambah Personnel',
                    handler:'onTambahPersonnel'
                }
                ]
});
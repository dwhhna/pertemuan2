Ext.define('Pertemuan.view.setting.BasicDataView', {
    extend: 'Ext.Container',
    xtype :'basicdataview',
    requires: [
        //'KitchenSink.model.Speaker',
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Pertemuan.store.Personnel',
        'Ext.field.Search',
    ],

    /*viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },*/

    
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    items: [{
            docked: 'top',
            xtype: 'toolbar',
            items: [
                {
                        xtype: 'searchfield',
                        placeHolder: 'Filter by nama',
                        name: 'searchfield',
                        listeners:[{
                        change: function (me,newValue,oldValue,eOpts){
                            //alert(newValue);
                            personnelStore = Ext.getStore('personnel');
                            personnelStore.filter('name', newValue);
                        }
                    }
                    ]
                }
            ]
        },{
            docked: 'top',
            xtype: 'toolbar',
            items: [
                {
                        xtype: 'searchfield',
                        placeHolder: 'Filter by npm ',
                        name: 'searchfield',
                        listeners:[{
                        change: function (me,newValue,oldValue,eOpts){
                            //alert(newValue);
                            personnelStore = Ext.getStore('personnel');
                            personnelStore.filter('npm', newValue);
                        }
                    }
                    ]
                }
            ]
        },{
            docked: 'top',
            xtype: 'toolbar',
            items: [
                {
                        xtype: 'searchfield',
                        placeHolder: 'Filter by Phone',
                        name: 'searchfield',
                        listeners:[{
                        change: function (me,newValue,oldValue,eOpts){
                            //alert(newValue);
                            personnelStore = Ext.getStore('personnel');
                            personnelStore.filter('phone', newValue);
                        }
                    }
                    ]
                }
            ]
        },{
        xtype: 'dataview',
        scrollable: 'y',
        cls: 'dataview-basic',
        itemTpl:'<table style="border-spacing:5px; border-collapse:separate">'+ 
        '<tr><td rowspan= 4><img class="image" src="{photo}" width=100></td>' +
        '<td><font size = 4><b>{name}</b><br>{npm}<br>{email}<br>{phone}</font></td>'+
        '<hr>',
        bind: {
            store: '{personnel}',
        },
        plugins: {
            type:'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            delegate: 'img',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
                    '<tr><td>NPM: </td><td>{npm}</td></tr>' +
                    '<tr><td>Nama: </td><td>{name}</td></tr>' +
                    '<tr><td>Email: </td><td>{email}</td></tr>' +
                    '<tr><td>Phone: </td><td>{phone}</td></tr>' 
                    
        }
    }]
});
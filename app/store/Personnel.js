 Ext.define('Pertemuan.store.Personnel', {
    extend: 'Ext.data.Store',   
    alias: 'store.personnel',
    autoLoad: true,
    autoSync: true,
    storeId:'personnel',

    fields: [
       'user_id','name','email','phone'
    ],

    proxy: {
        type: 'jsonp',
        api: {
         read: "http://localhost/MyApp_php/readPersonnel.php",
         update: "http://localhost/MyApp_php/updatePersonnel.php",
         destroy: "http://localhost/MyApp_php/destroyPersonnel.php",
         create: "http://localhost/MyApp_php/createPersonnel.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    },

    listeners: {
        beforeload: function(store,operation,eOpts){
            this.getProxy().setExtraParams({
                user_id: -1
            });
        }
    }
    
});  

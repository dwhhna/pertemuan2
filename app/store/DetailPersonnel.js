 Ext.define('Pertemuan.store.DetailPersonnel', {
    extend: 'Ext.data.Store',   
    alias: 'store.detailpersonnel',
    autoLoad: true,
    autoSync: true,
    storeId:'detailpersonnel',

    fields: [
       'user_id','name','email','phone'
    ],

    proxy: {
        type: 'jsonp',
        api: {
         read: "http://localhost/MyApp_php/readDetailPersonnel.php",
         update: "http://localhost/MyApp_php/updatePersonnel.php",
         destroy: "http://localhost/MyApp_php/destroyPersonnel.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
}); 


    Ext.define('Pertemuan.store.Bar', {
        extend: 'Ext.data.Store',
        alias: 'store.bar',
        autoLoad: true,
        autoSync: true,

        fields: ['barID','name', 'g1', 'g2', 'g3', 'g4', 'g5', 'g6' ],
        proxy: {
        type: 'jsonp',
        api: {
         read: "http://localhost/MyApp_php/readBar.php",
         update: "http://localhost/MyApp_php/updateBar.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
    });